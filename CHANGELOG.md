# CHANGELOG

## [1.2.1] - 2020-10-05
- Add current user change password

## [1.2.0] - 2020-09-14
- Use axios as peer dependency

## [1.1.0] - 2020-08-31
- Remove axios dependency
- Pass axio as parameter

## [1.0.0] - 2020-08-27
- Initial public version
- Authentication Vuex Store module
- Axios request interceptor

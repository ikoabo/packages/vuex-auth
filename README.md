# IKOA Business Opportunity Vuex Auth Integration Module

Vuex module to handle user authentication using IKOA Business Opportunity Identity Management Service. This library provides the vuext store state, actions, and mutations to handle security into VueJS UI Frontend applications.

The package is developed with the aim of being used in conjunction with the rest of the packages of the platform, but it don't restrict use it as standalone package.

[![Version npm](https://img.shields.io/npm/v/@ikoabo/vuex-auth.svg?style=flat-square)](https://www.npmjs.com/package/@ikoabo/vuex-auth)[![npm Downloads](https://img.shields.io/npm/dm/@ikoabo/vuex-auth.svg?style=flat-square)](https://npmcharts.com/compare/@ikoabo/vuex-auth?minimal=true)[![Build Status](https://gitlab.com/ikoabo/packages/vuex-auth/badges/master/pipeline.svg)](https://gitlab.com/ikoabo/packages/vuex-auth)[![coverage testing report](https://gitlab.com/ikoabo/packages/vuex-auth/badges/master/coverage.svg)](https://gitlab.com/ikoabo/packages/vuex-auth/-/commits/master)

[![NPM](https://nodei.co/npm/@ikoabo/vuex-auth.png?downloads=true&downloadRank=true)](https://nodei.co/npm/@ikoabo/vuex-auth/)

## Installation

```bash
npm install @ikoabo/vuex-auth
```

## Module configuration

The authententication middleware must be initialized with the address of the Identity Management Service

```js
import { setup as setupAuth } from "@ikoabo/vuex-auth";
const token = "Basic authentication token";
setupAuth(store, "https://myauth.example.com", token, true);
```

This initialization proces will register automatically the Vuex module under the namespace `auth`. Also this process will register an interceptor to `axios` HTTP client to allow injection of access tokens for authentication.

The `store` value must be set to the project Vuex store root, and the last parameter indicate is the axios interceptor must be registered or not. If you prefer create your own interceptor you must disable the registration.

## Vuex modules components

The module is composed of a set off getters, actions and mutations that allows call or dispatch actions on the Business Opportunity Identity Management Server.

Vuex module getters

- `auth/appAccessToken`: Fetch from the store module the application access token to be used to authenticate requests.
- `accessToken`: Fetch from the store module the authenticated user account access token to be used to authenticate requests and validate user account.
- `auth/isLogged`: Check if there is an user account authenticated.
- `auth/userScope`: Fetch all scopes owned by the current authenticated user. It can be used to dinamically show or hidden UI components and validate user permissions.
- `auth/profile`: Fetch the current authenticated user basic profile. This basic profile contains: 

  * `uid`: The user account unique identifier.
  * `name`: The user account registered name.
  * `lastname`: The user account registered lastname.
  * `email`: The user account email address used to authenticate. As the user account can hold many email address, this fields contains the email used to authenticate the active session.
  * `phone`: The user account registered phone.
  * `code`: The user account virtual code. This code is generated at user registration and its unique between all users. This code is used also to identificate the user account.

- `auth/isScope`: Check if the user holds an specific scope. The scope should be passed as function parameter.

Vuex module actions

- `auth/callApplicationSignin`: Call to authenticate the current application. This call exchange the basic application token by a bearer access token.

```js
this.$store.dispatch("auth/callApplicationSignin")
.then(()=>{
  // Application is authenticated
}).catch(err=>{
  // Handle error
})
```

- `auth/callSignin`: Call to authenticate an user account using its email address and password. Whe the user account is authenticated without errors the received access token is stored and the user profile loaded from server.

```js
this.$store.dispatch("auth/callSignin", {
  email: "email@example.com", 
  password: "mypassword"
}).then(()=>{
  // User account is authenticated
}).catch(err=>{
  // Handle error
})
```

- `auth/callSignup`: Register new user account into the server. This account can be registered with or without a referral code.

```js
this.$store.dispatch("auth/callSignup", {
  email: "email@example.com", 
  password: "mypassword", 
  name: "Jhon", 
  lastname: "Doe", 
  phone: "1234567890", 
  referral: "v12h32"
}).then(()=>{
  // User account is registered
}).catch(err=>{
  // Handle error
})
```

- `auth/callResendConfirmation`: Request the server to send a new email confirmation to an account that must be confirmed. To send a new request the user must be successfull authenticate in the server, so, valir email and password combination must be send.

```js
this.$store.dispatch("auth/callResendConfirmation", {
  email: "email@example.com", 
  password: "mypassword"
}).then(()=>{
  // User email confirmation sent
}).catch(err=>{
  // Handle error
})
```

- `auth/callForgotRequest`: Request a recover email to recover account password.

```js
this.$store.dispatch("auth/callForgotRequest", {
  email: "email@example.com", 
}).then(()=>{
  // Recover account was requested and email sent
}).catch(err=>{
  // Handle error
})
```

- `auth/callForgotCheck`: Validate the recover token to request the user account password.

```js
this.$store.dispatch("auth/callForgotCheck", {
  email: "email@example.com",
  token: "recovertoken"
}).then(()=>{
  // Recover token is valid
}).catch(err=>{
  // Handle error
})
```

- `auth/callForgotRecover`: Set the new password using the recover token.

```js
this.$store.dispatch("auth/callForgotRecover", {
  email: "email@example.com",
  token: "recovertoken",
  password: "newpassword"
}).then(()=>{
  // User account recovered
}).catch(err=>{
  // Handle error
})
```

- `auth/callReloadCredentials`: Reload the user account information validating the stored access token.

```js
this.$store.dispatch("auth/callReloadCredentials")
.then(()=>{
  // Access token is valid and user account is authenticated
}).catch(err=>{
  // Handle error
})
```


- `auth/callConfirm`: Confirm the user account with the given email address and validation token.

```js
this.$store.dispatch("auth/callConfirm",{
  email: "email@example.com",
  token: "recovertoken"
}).then(()=>{
  // User account confirmed
}).catch(err=>{
  // Handle error
})
```

- `auth/callProfile`: Get the current authenticated user account profile.

```js
this.$store.dispatch("auth/callProfile")
.then(()=>{
  // Current user account profile loaded
}).catch(err=>{
  // Handle error
})
```

- `auth/callLogout`: Close the current user account session.

```js
this.$store.dispatch("auth/callLogout")
.then(()=>{
  // Current user account session closed
}).catch(err=>{
  // Handle error
})
```

Vuex module mutations

- `auth/doApplicationSignin`: Store the authenticated application access token.

```js
this.$store.commit("auth/doApplicationSignin", {
  accessToken: "applicationaccesstoken"
});
```

- `auth/doSignin`: Store the authenticated user account information.

```js
this.$store.commit("auth/doSignin", {
  byPass: false,
  data: {
    accessToken: "currentuseraccesstoken",
    refreshToken: "currentuserrefreshtoken"
  }
});
```

If the `byPass` set to `true` then only base information is updated. The access and refresh token will keep the current value.

- `auth/doProfile`: Store the current user profile information.

```js
this.$store.commit("auth/doProfile", {
  uid: "useruid",
  name: "John",
  lastname: "Doe",
  email: "jdoe@example.com",
  phone: "123456789",
  code: "bn234h4"
});
```

- `auth/doReloadCredentials`: Load the current user account from browser storage.

```js
this.$store.commit("auth/doReloadCredentials");
```

- `auth/doLogout`: Clear the authenticated user account information.

```js
this.$store.commit("auth/doLogout");
```

## Inject authentication in axios requests

When the module is configured there is an interceptor that handle the authentication on every request. If the user prefer can use it own interceptor, disabling the default interceptor at initialization step. To use the interceptor only need to configure the `authorization` header:

```js
axios.post("https://url.example.com", body, {
  headers: {
    Authorization: "APP",
  },
})
```

The authorization values can be:

- `APP`: Use the application access token.
- `USER`: Use the authenticated user access token.

If the `authorization`header is not set then the interceptor try to use the user access token if there is a user authenticated or use the application access token.

If the request don't need to use any authentication then the header `noauth` must be set:

```js
axios.post("https://url.example.com", body, {
  headers: {
    noauth: 1,
  },
})
```

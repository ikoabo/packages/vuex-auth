/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */
import Vuex from "vuex"

/**
 * Authentication prototype error predefined constants
 */
export interface AUTH_ERRORS {
  [prop: string]: number
}

/**
 * Authentication prototype of service predefined endpoints
 */
export interface SERVICES {
  [prop: string]: number
}

/**
 * Module setup function
 * 
 * @param vuexStore 
 * @param authServer 
 * @param basicToken 
 */
export function setup(vuexStore: Vuex.Store, authServer: string, basicToken: string): void

/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */
import AuthService from "../services/auth.service"

/**
 * Handle auth server error response
 * 
 * @param {*} reject 
 */
function handleError(reject) {
  return (err) => {
    if (!err.response || !err.response.data || !err.response.data.error) {
      return reject(-9999)
    }
    reject(err.response.data.error)
  }
}

const state = {
  appToken: null,
  userToken: null,
  refresh: null,
  scope: [],
  profile: {},
}

const getters = {
  /* Get application access token */
  appAccessToken: state => {
    return state.appToken
  },
  /* Get user access token */
  accessToken: state => {
    return state.userToken
  },
  /* Check if there is an user authenticated */
  isLogged: state => {
    return state.userToken != null
  },
  /* Get the user scopes */
  userScope: state => {
    return state.scope
  },
  /* Get the user profile */
  profile: state => {
    return state.profile
  },
  /* Check if the current user holds an scope */
  isScope: (state) => {
    return (value) => {
      if (!value) {
        return true
      }
      return state.scope.indexOf(value) >= 0
    }
  }
}

const actions = {
  /**
   * Call to authenticate the current application
   * 
   * @param {*} context 
   */
  callApplicationSignin(context) {
    return new Promise((resolve, reject) => {
      AuthService.applicationSignin()
        .then(res => {
          context.commit("doApplicationSignin", res.data)
          resolve()
        }).catch(handleError(reject))
    })
  },

  /**
   * Call to authenticate an user account
   * 
   * @param {*} context 
   * @param {*} payload 
   */
  callSignin(context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.signin(payload.email, payload.password)
        .then(res => {
          context.commit("doSignin", { byPass: false, data: res.data })
          /* Call to get user profile */
          context.dispatch("callProfile").then(resolve).catch(reject)
        }).catch(handleError(reject))
    })
  },

  /**
   * Call to register new user account
   * 
   * @param {*} _context 
   * @param {*} payload 
   */
  callSignup(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.signup(payload.email, payload.password, payload.name, payload.lastname, payload.phone, payload.referral)
        .then(() => {
          resolve()
        }).catch(handleError(reject))
    })
  },

  /**
   * Call to resend email confirmation link
   * 
   * @param {*} _context 
   * @param {*} payload 
   */
  callResendConfirmation(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.resendConfirmation(payload.email, payload.password)
        .then(resolve).catch(handleError(reject))
    })
  },

  /**
   * Call to request a recover mail
   * 
   * @param {*} _context 
   * @param {*} payload 
   */
  callForgotRequest(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.forgotRequest(payload.email)
        .then(resolve).catch(handleError(reject))
    })
  },

  /**
   * Call to verify recover token
   * 
   * @param {*} _context 
   * @param {*} payload 
   */
  callForgotCheck(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.forgotCheck(payload.email, payload.token)
        .then(resolve).catch(handleError(reject))
    })
  },

  /**
   * Call to set the new password after recover
   * 
   * @param {*} _context 
   * @param {*} payload 
   */
  callForgotRecover(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.forgotRecover(payload.email, payload.token, payload.password)
        .then(resolve).catch(handleError(reject))
    })
  },

  /**
   * Call to reload user credentials from browser storage
   * 
   * @param {*} context 
   */
  callReloadCredentials(context) {
    return new Promise((resolve, reject) => {
      /* Reload credentials from storage */
      context.commit("doReloadCredentials")

      /* Bypass validation if there is not credentials */
      const token = context.state.userToken
      if (!token) {
        resolve()
        return
      }

      /* Validate the credentials */
      AuthService.verifyToken(token)
        .then(res => {
          context.commit("doSignin", { byPass: true, data: res.data })

          /* Load user profile */
          context.dispatch("callProfile")
            .then(resolve)
            .catch(reject)
        }).catch(handleError(reject))
    })
  },

  /**
   * Call to confirm user account email
   * @param {*} _context 
   * @param {*} payload 
   */
  callConfirm(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.confirm(payload.email, payload.token)
        .then(resolve).catch(handleError(reject))
    })
  },

  /**
   * Call to get current user profile
   * 
   * @param {*} context 
   */
  callProfile(context) {
    return new Promise((resolve, reject) => {
      AuthService.profile()
        .then(res => {
          context.commit("doProfile", res.data)
          resolve()
        }).catch(handleError(reject))
    })
  },

  /**
   * Call to logout current user account
   * 
   * @param {*} context 
   */
  callLogout(context) {
    return new Promise(resolve => {
      AuthService.logout().finally(() => {
        context.commit("doLogout")
        resolve()
      })
    })
  },

  /**
   * Call to change current user password
   * @param {*} _context 
   * @param {*} payload 
   */
  callChangePassword(_context, payload) {
    return new Promise((resolve, reject) => {
      AuthService.changePassword(payload.oldPassword, payload.newPassword)
        .then(resolve).catch(handleError(reject))
    })
  },

}

const mutations = {
  /**
   * Store the application token
   * 
   * @param {*} state 
   * @param {*} payload 
   */
  doApplicationSignin(state, payload) {
    state.appToken = payload.accessToken
  },

  /**
   * Store the user authentication information
   * 
   * @param {*} state 
   * @param {*} payload 
   */
  doSignin(state, payload) {
    if (!payload.byPass) {
      state.userToken = payload.data.accessToken
      state.refresh = payload.data.refreshToken
      localStorage.setItem("utk", state.userToken || "")
      localStorage.setItem("rtk", state.refresh || "")
    }
    state.scope = payload.data.scope
  },

  /**
   * Store the current user profile
   * 
   * @param {*} state 
   * @param {*} payload 
   */
  doProfile(state, payload) {
    state.profile = {
      uid: payload.uid,
      name: payload.name,
      lastname: payload.lastname,
      email: payload.email,
      phone: payload.phone,
      code: payload.code
    }
  },

  /**
   * Reload the user browser stored credentials
   * 
   * @param {*} state 
   */
  doReloadCredentials(state) {
    state.userToken = localStorage.getItem("utk")
    state.refresh = localStorage.getItem("rtk")
  },

  /**
   * Clear the active user information
   * 
   * @param {*} state 
   */
  doLogout(state) {
    state.userToken = null
    state.refresh = null
    state.scope = []
    state.profile = {}
    localStorage.removeItem("utk")
    localStorage.removeItem("rtk")
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}

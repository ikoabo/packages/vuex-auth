/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */

/**
 * Predefined auth service endpoints
 */
export const SERVICES = {
  SIGNUP: "/v1/oauth/signup",
  SIGNIN: "/v1/oauth/signin",
  RESEND: "/v1/oauth/resend",
  VERIFY: "/v1/oauth/authenticate",
  CONFIRM: "/v1/oauth/confirm",
  FORGOT: "/v1/oauth/recover/request",
  FORGOT_CHECK: "/v1/oauth/recover/validate",
  FORGOT_RECOVER: "/v1/oauth/recover/store",
  PROFILE: "/v1/oauth/profile",
  CHANGE_PASSWORD: "/v1/oauth/password",
  LOGOUT: "/v1/oauth/logout",
}

/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */

/**
 * Authentication server errors
 */
export const AUTH_ERRORS = {
  INVALID_AUTH_SERVER: 1001,
  UNKNOWN_AUTH_SERVER_ERROR: 1002,
  INVALID_SERVER_RESPONSE: 1003,
  INVALID_AUTHORIZATION_CODE: 1004,
  INVALID_TOKEN: 1005,
  INVALID_REFRESH_TOKEN: 1006,
  INVALID_SCOPE: 1007,
  INVALID_DOMAIN: 1008,
  INVALID_PROJECT: 1009,
  INVALID_APPLICATION: 1010,
  TOKEN_EXPIRED: 1011,
  MAX_ATTEMPTS: 1012,
  NOT_ALLOWED_SIGNIN: 1013,
  ACCOUNT_DISABLED: 1014,
  ACCOUNT_CANCELLED: 1015,
  ACCOUNT_BLOCKED: 1016,
  ACCOUNT_NOT_REGISTERED: 1017,
  ACCOUNT_ALREADY_CONFIRMED: 1018,
  EMAIL_NOT_CONFIRMED: 1019,
  INVALID_CREDENTIALS: 1020,
  EMAIL_IN_USE: 1021,
  USER_DUPLICATED: 1022,
  PROFILE_NOT_FOUND: 1023,
  RECOVER_NOT_ALLOWED: 1024,
  AUTHENTICATION_REQUIRED: 1025,
  APPLICATION_RESTRICTED: 1026,
  INVALID_CODE_ERROR: 1027,
  INVALID_CODE_FULL: 1028,
  INVALID_SOCIAL_REQUEST: 1029,
  USER_ACCOUNT_MISMATCH: 1030,
  USER_SOCIAL_MISMATCH: 1031,
  ACCOUNT_PROFILE_NOT_FOUND: 1032,
  CANT_REGISTER_ANOTHER_SOCIAL: 1033,
}

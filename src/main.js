/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */
import AuthModule from "./modules/auth.module"
import { setupService } from "./services/auth.service"
import axios from "axios"

/* Export module settings */
export { AUTH_ERRORS } from "./configs/errors.config"
export { SERVICES } from "./configs/services.config"

/* Export VUEX module */


/**
 * Utility function to initialize the module
 * 
 * @param {*} vuexStore 
 * @param {*} basicToken 
 */
export function setup(vuexStore, authServer, basicToken, useInterceptor) {

  /* Dynamically register the Vuex Module */
  vuexStore.registerModule("auth", AuthModule)

  /* Setup the authentication service */
  setupService(authServer)

  /* Check if the axios interceptor must be avoided */
  if (!useInterceptor) {
    return;
  }

  /**
   * AXIOS interceptor to inject authentication into requests
   */
  axios.interceptors.request.use((request) => {
    /* Append content type header if its not present */
    if (!request.headers["Content-Type"]) {
      request.headers["Content-Type"] = "application/json"
    }

    /* Check if the request must bypass the Authorization header */
    if (request.headers.noauth) {
      delete request.headers.Authorization
      return request
    }

    /* Check if authorization is set */
    if (!request.headers["Authorization"]) {
      /* Check if the user is authenticated to send Bearer token */
      const token = vuexStore.getters["auth/accessToken"]
      if (token && token.length > 0) {
        request.headers.Authorization = "Bearer " + token
      } else {
        /* Send the application authentication as Bearer token */
        request.headers.Authorization = "Bearer " + vuexStore.getters["auth/appAccessToken"]
      }
    } else {
      const auth = request.headers["Authorization"]
      if (auth === "APP") {
        /* Send the application authentication as Bearer token */
        request.headers.Authorization = "Bearer " + vuexStore.getters["auth/appAccessToken"]
      } else if (auth === "USER") {
        /* Send the authenticated user token */
        request.headers.Authorization = "Bearer " + vuexStore.getters["auth/accessToken"]
      } else if (auth === "BASIC") {
        /* Send the application basic token */
        request.headers.Authorization = "Basic " + basicToken
      }
    }
    return request
  })
}
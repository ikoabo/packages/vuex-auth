/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * Author: Reinier Millo SÃ¡nchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity Auth API.
 */
import { SERVICES } from "../configs/services.config"
import axios from "axios"

let AUTH_SERVER = ""

/**
 * Prepare the auth server to be called
 * 
 * @param {*} endPoint 
 */
function callAuth(endPoint) {
  return `${AUTH_SERVER}${SERVICES[endPoint]}`
}

/**
 * Setup the auth server setting
 * 
 * @param {*} authServer 
 */
export function setupService(authServer) {
  AUTH_SERVER = authServer
}

export default {
  /**
   * Authenticate the web application to get an access token
   */
  applicationSignin() {
    const body = new URLSearchParams()
    body.append("grant_type", "client_credentials")
    return axios.post(callAuth("SIGNIN"), body, {
      headers: {
        Authorization: 'BASIC',
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
  },

  /**
   * Authenticate an user account
   */
  signin(email, password) {
    const body = new URLSearchParams()
    body.append("grant_type", "password")
    body.append("username", email)
    body.append("password", password)
    return axios.post(callAuth("SIGNIN"), body, {
      headers: {
        Authorization: 'BASIC',
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
  },

  /**
   * Register new user account
   */
  signup(email, password, name, lastname, phone, referral) {
    return axios.post(callAuth("SIGNUP"), {
      email: email,
      password: password,
      name: name,
      lastname: lastname,
      phone: phone,
      referral: referral || "",
    }, {
      headers: {
        Authorization: "APP",
      },
    })
  },

  /**
   * Validate the stored user credentials
   */
  verifyToken(token) {
    return axios.post(callAuth("VERIFY"), {}, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  },

  /**
   * Resend user account confirmation email
   */
  resendConfirmation(email, password) {
    const body = new URLSearchParams()
    body.append("grant_type", "password")
    body.append("username", email)
    body.append("password", password)
    return axios.post(callAuth("RESEND"), body, {
      headers: {
        Authorization: 'BASIC',
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
  },

  /**
   * Confirm the user account
   */
  confirm(email, token) {
    return axios.post(callAuth("CONFIRM"), {
      email: email,
      token: token,
    }, {
      headers: {
        Authorization: "APP",
      },
    })
  },

  /**
   * Request a recover link to recover password account
   */
  forgotRequest(email) {
    return axios.post(callAuth("FORGOT"), {
      email: email
    }, {
      headers: {
        Authorization: "APP",
      },
    })
  },

  /**
   * Validate the recover token
   */
  forgotCheck(email, token) {
    return axios.post(callAuth("FORGOT_CHECK"), {
      email: email,
      token: token,
    }, {
      headers: {
        Authorization: "APP",
      },
    })
  },

  /**
   * Set the new password for the recovered account
   */
  forgotRecover(email, token, password) {
    return axios.post(callAuth("FORGOT_RECOVER"), {
      email: email,
      token: token,
      password: password
    }, {
      headers: {
        Authorization: "APP",
      },
    })
  },

  /**
   * Logout the current user
   */
  logout() {
    return axios.post(callAuth("LOGOUT"), {}, {
      headers: {
        Authorization: "USER",
      },
    })
  },

  /**
   * Fetch current user base profile
   */
  profile() {
    return axios.get(callAuth("PROFILE"), {
      headers: {
        Authorization: "USER",
      },
    })
  },

  /**
   * Change user password
   */
  changePassword(oldPassword, newPassword) {
    return axios.put(callAuth("CHANGE_PASSWORD"), {
      oldPassword: oldPassword,
      newPassword: newPassword
    }, {
      headers: {
        Authorization: "USER",
      },
    })
  },
}
